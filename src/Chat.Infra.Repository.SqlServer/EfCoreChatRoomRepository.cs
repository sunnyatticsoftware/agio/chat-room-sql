﻿using Chat.Application.Contracts;
using Chat.Domain;
using Microsoft.EntityFrameworkCore;

namespace Chat.Infra.Repository.SqlServer;

public class EfCoreChatRoomRepository
    : IChatRoomRepository
{
    private readonly ChatRoomDbContext _chatRoomDbContext;
    private readonly IChatRoomFactory _chatRoomFactory;

    public EfCoreChatRoomRepository(
        ChatRoomDbContext chatRoomDbContext,
        IChatRoomFactory chatRoomFactory)
    {
        _chatRoomDbContext = chatRoomDbContext;
        _chatRoomFactory = chatRoomFactory;
    }
    
    public async Task<ChatRoom> GetById(Guid id)
    {
        var chatEntity = await
            _chatRoomDbContext
                .Chats
                .Include(x => x.Users)
                .ThenInclude(u => u.Messages)
                .SingleOrDefaultAsync(x => x.Id == id);
        if (chatEntity is null)
        {
            throw new KeyNotFoundException($"Could not find chat room with Id {id}");
        }

        var userEntities = chatEntity.Users;
        var users = userEntities.Select(Mappers.MapToUser).ToList();
        var messageEntities = chatEntity.Users.SelectMany(x => x.Messages);
        var messages = messageEntities.Select(Mappers.MapToMessage).ToList();

        var chatRoom = _chatRoomFactory.Create(id);
        // Reflection to set internal values
        ReflectionUtil.SetValue(chatRoom, users);
        ReflectionUtil.SetValue(chatRoom, messages);
        return chatRoom;
    }

    public async Task Save(ChatRoom chatRoom)
    {
        await RemoveChat(chatRoom.Id);
        var chatEntity = Mappers.MapToChatEntity(chatRoom);
        await _chatRoomDbContext.Chats.AddAsync(chatEntity);
        await _chatRoomDbContext.SaveChangesAsync();
    }

    private async Task RemoveChat(Guid id)
    {
        var chat = _chatRoomDbContext.Chats.SingleOrDefault(x => x.Id == id);
        if (chat != null)
        {
            _chatRoomDbContext.Remove(chat);
            await _chatRoomDbContext.SaveChangesAsync();
        }
    }
    
    
}