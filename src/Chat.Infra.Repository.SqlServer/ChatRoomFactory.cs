using Chat.Domain;
using Chat.Domain.Contracts;

namespace Chat.Infra.Repository.SqlServer;

public class ChatRoomFactory
{
    private readonly IDateTimeFactory _dateTimeFactory;
    private readonly INotificationService _notificationService;

    public ChatRoomFactory(IDateTimeFactory dateTimeFactory, INotificationService notificationService)
    {
        _dateTimeFactory = dateTimeFactory;
        _notificationService = notificationService;
    }

    public ChatRoom Create(Guid id)
    {
        var chatRoom = new ChatRoom(id, _dateTimeFactory, _notificationService);
        return chatRoom;
    }
}