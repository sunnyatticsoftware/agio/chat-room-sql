﻿using Chat.Application.Models;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Chat.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddOpenApi(this IServiceCollection services)
    {
        var mainAssemblyName = typeof(Startup).Assembly.GetName().Name;
        var applicationAssemblyName = typeof(UserDto).Assembly.GetName().Name;

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = mainAssemblyName, 
                Version = "v1",
                Description = "Chat Room Sample",
                Contact = new OpenApiContact
                {
                    Name = "Diego Martin",
                    Email = "diego.martin@sunnyatticsoftware.com"
                }
            });
            
            c.DocumentFilter<LowerCaseDocumentFilter>();
                
            var xmlCommentsWebApi = Path.Combine(AppContext.BaseDirectory, $"{mainAssemblyName}.xml");
            c.IncludeXmlComments(xmlCommentsWebApi);
            var xmlCommentsApplication = Path.Combine(AppContext.BaseDirectory, $"{applicationAssemblyName}.xml");
            c.IncludeXmlComments(xmlCommentsApplication);
        });
            
        return services;
    }
}

// ReSharper disable once ClassNeverInstantiated.Global
public class LowerCaseDocumentFilter
    : IDocumentFilter
{
    private static string LowercaseEverythingButParameters(string key) => string.Join('/', key.Split('/').Select(x => x.Contains("{") ? x : x.ToLower()));
    
    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
        var paths = swaggerDoc.Paths.ToDictionary(entry => LowercaseEverythingButParameters(entry.Key),
            entry => entry.Value);
        swaggerDoc.Paths = new OpenApiPaths();
        foreach (var (key, value) in paths)
        {
            swaggerDoc.Paths.Add(key, value);
        }
    }
}