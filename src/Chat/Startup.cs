using Chat.Application;
using Chat.Application.Contracts;
using Chat.Domain.Contracts;
using Chat.Extensions;
using Chat.Infra.Notifier.Websocket;
using Chat.Infra.Repository.InMemory;
using Chat.Infra.Repository.SqlServer;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using ChatRoomFactory = Chat.Application.ChatRoomFactory;

namespace Chat;

public class Startup
{
    private readonly IConfiguration _configuration;

    public Startup(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    
    public void ConfigureServices(IServiceCollection services)
    {
        var chatsConnectionString = _configuration.GetValue<string>("ConnectionStrings:ChatsDatabase");

        services
            .AddSingleton<IDateTimeFactory, DateTimeFactory>()
            .AddTransient<ChatCommandService>()
            .AddTransient<ChatQueryService>()
            // EF Core migrations and connection
            .AddDbContext<ChatRoomDbContext>(
                options =>
                {
                    options.UseSqlServer(chatsConnectionString);
                })
            // Dapper connection
             .AddTransient(
                 _ =>
                 {
                     var sqlConnection = new SqlConnection(chatsConnectionString);
                     return sqlConnection;
                 })
            // .AddSingleton<IChatRoomRepository, InMemoryChatRoomRepository>() // in memory repository
            // .AddTransient<IChatRoomRepository, EfCoreChatRoomRepository>() // EF Core repository
            .AddTransient<IChatRoomRepository, DapperChatRoomRepository>() // Dapper repository
            .AddSingleton<IChatRoomFactory, ChatRoomFactory>()
            .AddSingleton<IIdFactory, IdFactory>()
            .AddTransient<INotificationService>(sp => new NotificationService(NotificationServiceConfiguration.Default))
            .AddOpenApi()
            .AddControllers();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseRouting();
        app.UseOpenApi();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}