using Chat.Domain.Contracts;
using System.Net.Http.Json;

namespace Chat.Infra.Notifier.Websocket;

public class NotificationService
    : INotificationService
{
    private readonly HttpClient _httpClient;

    public NotificationService(NotificationServiceConfiguration notificationServiceConfiguration)
    {
        _httpClient = new HttpClient();
        _httpClient.BaseAddress = new Uri(notificationServiceConfiguration.Host);
    }
    
    public async Task SendNotification(string connectionId, string message)
    {
        var jsonObject =
            new
            {
                Notification = message
            };
        var result = await _httpClient.PostAsJsonAsync($"@connections/{connectionId}", jsonObject);
        if (!result.IsSuccessStatusCode)
        {
            // Do something?
        }
    }
}

public class NotificationServiceConfiguration
{
    public string Host { get; init; } = "ws://localhost:8080/ws";
    
    public static NotificationServiceConfiguration Default => new();
}