using Chat.Domain.Contracts;

namespace Chat.Application;

public class DateTimeFactory
    : IDateTimeFactory
{
    public DateTime CreateUtcNow()
    {
        return DateTime.UtcNow;
    }
}