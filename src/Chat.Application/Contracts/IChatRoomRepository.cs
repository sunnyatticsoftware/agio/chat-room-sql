using Chat.Domain;

namespace Chat.Application.Contracts;

public interface IChatRoomRepository
{
    Task<ChatRoom> GetById(Guid id);
    Task Save(ChatRoom chatRoom);
}