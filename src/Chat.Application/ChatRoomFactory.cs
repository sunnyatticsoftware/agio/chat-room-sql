using Chat.Application.Contracts;
using Chat.Domain;
using Chat.Domain.Contracts;

namespace Chat.Application;

public class ChatRoomFactory
    : IChatRoomFactory
{
    private readonly IIdFactory _idFactory;
    private readonly IDateTimeFactory _dateTimeFactory;
    private readonly INotificationService _notificationService;

    public ChatRoomFactory(
        IIdFactory idFactory,
        IDateTimeFactory dateTimeFactory,
        INotificationService notificationService)
    {
        _idFactory = idFactory;
        _dateTimeFactory = dateTimeFactory;
        _notificationService = notificationService;
    }
    
    // This has been refactored to satisfy Dapper and EF repositories when Id is known
    public ChatRoom Create()
    {
        var id = _idFactory.Create();
        var chatRoom = Create(id);
        return chatRoom;
    }

    public ChatRoom Create(Guid id)
    {
        var chatRoom = new ChatRoom(id, _dateTimeFactory, _notificationService);
        return chatRoom;
    }
}