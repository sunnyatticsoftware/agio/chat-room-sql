# chat-room-websocket-dapper

Continuación del ejemplo anterior de un chat con envío de mensajes en tiempo real.

Esta vez vamos a utilizar persistencia en base de datos, no en memoria.

## SASW Websocket Tester
Sigue siendo necesario utilizar un websocket tester para ejecutar los tests y la aplicación.
```
docker run -p 8080:8080 --name sasw-websocket-tester registry.gitlab.com/sunnyatticsoftware/sasw-community/sasw-websocket-tester:latest
```

## SQL Server

Para tener un servidor SQL Server corriendo como contenedor docker y credenciales `sa / Ag!oTra!ning2022` lanzamos el siguiente comando.
```
docker run -e 'ACCEPT_EULA=Y' -e 'MSSQL_SA_PASSWORD=Ag!oTra!ning2022' -p 1433:1433 --name sqlserver2019 -d mcr.microsoft.com/mssql/server:2019-latest
```

Si hay algún error, se pueden consultar las trazas con `docker logs sqlserver2019`

Opcionalmente instala una extensión para VSCODE llamada SQL Server (mssql) para conectar y ver base de datos. Crea una nueva conexión a `localhost` con SQL login `sa / Ag!oTra!ning2022`

## Migraciones
La inicial se crea con
``` 
dotnet ef migrations add inicial --project src/Chat.Infra.Repository.SqlServer --startup-project src/Chat
```

Un directoro llamado `Migrations` será automáticamente creado.
Para aplicar las migraciones generadas se ejecuta:
``` 
dotnet ef database update --project src/Chat.Infra.Repository.SqlServer --startup-project src/Chat
```