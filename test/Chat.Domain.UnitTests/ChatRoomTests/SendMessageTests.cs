using Chat.Domain.Contracts;
using Chat.Domain.UnitTests.TestSupport;
using Chat.Domain.UnitTests.TestSupport.Builders;
using FluentAssertions;
using Moq;

namespace Chat.Domain.UnitTests.ChatRoomTests;

public static class SendMessageTests
{
    public class Should_Send_Message
        : Given_When_Then_Async
    {
        private ChatRoom _sut = null!;
        private User _user = null!;
        private string _username = null!;
        private string _text = null!;
        private Exception _exception = null!;
        private Mock<INotificationService> _notificationServiceMock = null!;
        private string _expectedMessage = null!;

        protected override Task Given()
        {
            _username = "foo";
            _text = "hello";
            _user = new User(_username, "bar");

            var createdOn = new DateTime(2022, 1, 1, 1, 1, 1); // 2022-01-01 01:01:01
            
            var dateTimeFactoryMock = new Mock<IDateTimeFactory>();
            dateTimeFactoryMock
                .Setup(x => x.CreateUtcNow())
                .Returns(createdOn);

            var createdOnFormatted = createdOn.ToString("yyyy-MM-dd HH:mm:ss");
            
            _expectedMessage = $"[{createdOnFormatted}] {_username} says: {_text}";

            _notificationServiceMock = new Mock<INotificationService>();
            _notificationServiceMock
                .Setup(x => x.SendNotification(_user.ConnectionId, _expectedMessage))
                .Returns(Task.CompletedTask);
            
            _sut =
                new ChatRoomBuilder()
                    .WithDateTimeFactory(dateTimeFactoryMock.Object)
                    .WithNotificationService(_notificationServiceMock.Object)
                    .Object;
            
            _sut.Join(_user);

            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            try
            {
                await _sut.SendMessage(_username, _text);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_Any_Exception()
        {
            _exception.Should().BeNull();
        }

        [Fact]
        public void Then_It_Should_Send_The_Expected_Notification()
        {
            _notificationServiceMock.Verify(x => x.SendNotification(_user.ConnectionId, _expectedMessage), Times.Once);
        }
    }
    
    public class Should_Not_Send_Message_From_Non_Existing_Users
        : Given_When_Then_Async
    {
        private ChatRoom _sut = null!;
        private string _username = null!;
        private string _text = null!;
        private Exception _exception = null!;
        private string _expectedExceptionMessage = null!;

        protected override Task Given()
        {
            _username = "foo";
            _text = "hello";
            _sut =
                new ChatRoomBuilder()
                    .Object;

            _expectedExceptionMessage = "User with username foo not joined";

            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            try
            {
                await _sut.SendMessage(_username, _text);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Throw_An_Exception()
        {
            _exception.Should().NotBeNull();
        }

        [Fact]
        public void Then_It_Should_Be_An_ArgumentException()
        {
            _exception.Should().BeAssignableTo<ArgumentException>();
        }

        [Fact]
        public void Then_It_Should_Have_The_Expected_Exception_Message()
        {
            _exception.Message.Should().Be(_expectedExceptionMessage);
        }
    }
    
    public class Should_Send_Message_From_A_User_To_All_Users_In_Chat_Room
        : Given_When_Then_Async
    {
        private ChatRoom _sut = null!;
        private Exception _exception = null!;
        private Mock<INotificationService> _notificationServiceMock = null!;
        private string _expectedMessage = null!;
        private string _senderUsername = null!;
        private string _text = null!;

        protected override Task Given()
        {
            var userOne = new User("one", "connectionOne");
            var userTwo = new User("two", "connectionTwo");
            var userThree = new User("three", "connectionThree");

            var createdOn = new DateTime(2022, 1, 1, 1, 1, 1); // 2022-01-01 01:01:01
            var createdOnFormatted = createdOn.ToString("yyyy-MM-dd HH:mm:ss");
            
            var dateTimeFactoryMock = new Mock<IDateTimeFactory>();
            dateTimeFactoryMock
                .Setup(x => x.CreateUtcNow())
                .Returns(createdOn);

            _senderUsername = userTwo.Username;
            _text = "Hello world";
            _expectedMessage = $"[{createdOnFormatted}] {_senderUsername} says: Hello world";
            
            _notificationServiceMock = new Mock<INotificationService>();
            _notificationServiceMock
                .Setup(x => x.SendNotification(It.IsAny<string>(), _expectedMessage))
                .Returns(Task.CompletedTask);

            _sut =
                new ChatRoomBuilder()
                    .WithDateTimeFactory(dateTimeFactoryMock.Object)
                    .WithNotificationService(_notificationServiceMock.Object)
                    .Object;
            
            _sut.Join(userOne);
            _sut.Join(userTwo);
            _sut.Join(userThree);

            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            try
            {
                await _sut.SendMessage(_senderUsername, _text);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_Any_Exception()
        {
            _exception.Should().BeNull();
        }

        [Fact]
        public void Then_It_Should_Send_The_Expected_Notification_Three_Times()
        {
            _notificationServiceMock.Verify(x => x.SendNotification(It.IsAny<string>(), _expectedMessage), Times.Exactly(3));
        }
    }
    
    public class Should_Send_Message_From_A_User_To_All_Unique_Users_In_Chat_Room
        : Given_When_Then_Async
    {
        private ChatRoom _sut = null!;
        private Exception _exception = null!;
        private Mock<INotificationService> _notificationServiceMock = null!;
        private string _expectedMessage = null!;
        private string _senderUsername = null!;
        private string _text = null!;
        private List<string> _connectionIdsNotified = null!;
        private List<string> _expectedConnectionIdsNotified = null!;

        protected override Task Given()
        {
            var userOne = new User("one", "connectionOne");
            var userTwo = new User("two", "connectionTwo");
            var userThree = new User("three", "connectionThree");
            var userOneUpdated = new User("one", "connectionFour");

            var createdOn = new DateTime(2022, 1, 1, 1, 1, 1); // 2022-01-01 01:01:01
            var createdOnFormatted = createdOn.ToString("yyyy-MM-dd HH:mm:ss");
            
            var dateTimeFactoryMock = new Mock<IDateTimeFactory>();
            dateTimeFactoryMock
                .Setup(x => x.CreateUtcNow())
                .Returns(createdOn);

            _senderUsername = userTwo.Username;
            _text = "Hello world";
            _expectedMessage = $"[{createdOnFormatted}] {_senderUsername} says: Hello world";

            _connectionIdsNotified = new List<string>();
            
            _notificationServiceMock = new Mock<INotificationService>();
            _notificationServiceMock
                .Setup(x => x.SendNotification(It.IsAny<string>(), _expectedMessage))
                .Returns(Task.CompletedTask)
                .Callback(new Action<string, string>((connectionId, message) =>
                {
                    // the parameters map the arguments send to the mock, so we can capture them
                    _connectionIdsNotified.Add(connectionId);
                }));

            _sut =
                new ChatRoomBuilder()
                    .WithDateTimeFactory(dateTimeFactoryMock.Object)
                    .WithNotificationService(_notificationServiceMock.Object)
                    .Object;
            
            _sut.Join(userOne);
            _sut.Join(userTwo);
            _sut.Join(userThree);
            _sut.Join(userOneUpdated);

            _expectedConnectionIdsNotified =
                new List<string>
                {
                    userTwo.ConnectionId,
                    userThree.ConnectionId,
                    userOneUpdated.ConnectionId
                };

            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            try
            {
                await _sut.SendMessage(_senderUsername, _text);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_Any_Exception()
        {
            _exception.Should().BeNull();
        }

        [Fact]
        public void Then_It_Should_Send_The_Expected_Notification_Three_Times()
        {
            _notificationServiceMock.Verify(x => x.SendNotification(It.IsAny<string>(), _expectedMessage), Times.Exactly(3));
        }
        
        [Fact]
        public void Then_It_Should_Send_The_Expected_Notification_Only_To_Expected_Connection_Ids()
        {
            _connectionIdsNotified.Should().BeEquivalentTo(_expectedConnectionIdsNotified, config => config.WithStrictOrdering());
        }
    }
}