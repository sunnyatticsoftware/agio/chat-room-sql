using Chat.Application.Contracts;
using Chat.Application.UnitTests.TestSupport;
using Chat.Application.UnitTests.TestSupport.Builders;
using Chat.Domain;
using FluentAssertions;

namespace Chat.Application.UnitTests.ChatRoomFactoryTests;

public static class CreateTests
{
    public class Should_Create_Chat_Room
        : Given_When_Then
    {
        private ChatRoomFactory _sut = null!;
        private ChatRoom _result = null!;

        protected override void Given()
        {
            _sut =
                new ChatRoomFactoryBuilder()
                    .Object;
        }

        protected override void When()
        {
            _result = _sut.Create();
        }

        [Fact]
        public void Then_It_Should_Be_An_IChatRoomFactory()
        {
            _sut.Should().BeAssignableTo<IChatRoomFactory>();
        }

        [Fact]
        public void Then_It_Should_Return_A_Valid_ChatRoom()
        {
            _result.Should().NotBeNull();
        }
    }
    
    public class Should_Create_Unique_Chat_Room
        : Given_When_Then
    {
        private ChatRoomFactory _sut = null!;
        private ChatRoom _result = null!;
        private ChatRoom _oldChatRoom = null!;

        protected override void Given()
        {
            var idFactory = new IdFactory();
            _sut =
                new ChatRoomFactoryBuilder()
                    .WithIdFactory(idFactory)
                    .Object;

            _oldChatRoom = _sut.Create();
        }

        protected override void When()
        {
            _result = _sut.Create();
        }

        [Fact]
        public void Then_It_Should_Create_Unique_ChatRoom()
        {
            _result.Id.Should().NotBe(_oldChatRoom.Id);
        }
    }
}