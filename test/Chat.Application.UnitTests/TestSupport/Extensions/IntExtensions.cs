namespace Chat.Application.UnitTests.TestSupport.Extensions;

public static class IntExtensions
{
    public static Guid ToGuid(this int value)
    {
        byte[] b = new byte[16];
        BitConverter.GetBytes(value).CopyTo((Array) b, 0);
        return new Guid(b);
    }

    public static DateTime ToUtcDate(this int milliseconds)
    {
        var seed = new DateTime(2022, 1, 1, 1, 1, 1, DateTimeKind.Utc);
        var result = seed.AddMilliseconds(milliseconds);
        return result;
    }
}