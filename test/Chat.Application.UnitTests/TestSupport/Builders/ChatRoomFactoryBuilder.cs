using Chat.Application.Contracts;
using Chat.Domain.Contracts;
using Moq;

namespace Chat.Application.UnitTests.TestSupport.Builders;

public class ChatRoomFactoryBuilder
{
    private IIdFactory _idFactory = Mock.Of<IIdFactory>();
    private IDateTimeFactory _dateTimeFactory = Mock.Of<IDateTimeFactory>();
    private INotificationService _notificationService = Mock.Of<INotificationService>();

    public ChatRoomFactoryBuilder WithIdFactory(IIdFactory idFactory)
    {
        _idFactory = idFactory;
        return this;
    }
    
    public ChatRoomFactoryBuilder WithDateTimeFactory(IDateTimeFactory dateTimeFactory)
    {
        _dateTimeFactory = dateTimeFactory;
        return this;
    }
    
    public ChatRoomFactoryBuilder WithNotificationService(INotificationService notificationService)
    {
        _notificationService = notificationService;
        return this;
    }

    public ChatRoomFactory Object => new ChatRoomFactory(_idFactory, _dateTimeFactory, _notificationService);
}