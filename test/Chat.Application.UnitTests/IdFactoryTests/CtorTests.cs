using Chat.Application.Contracts;
using Chat.Application.UnitTests.TestSupport;
using FluentAssertions;

namespace Chat.Application.UnitTests.IdFactoryTests;

public static class CtorTests
{
    public class Should_Instantiate_IdFactory
        : Given_When_Then
    {
        private IdFactory _sut = null!;

        protected override void Given()
        {
        }

        protected override void When()
        {
            _sut = new IdFactory();
        }

        [Fact]
        public void Then_It_Should_Have_A_Valid_Instance()
        {
            _sut.Should().NotBeNull();
        }

        [Fact]
        public void Then_It_Should_Be_An_IIdFactory()
        {
            _sut.Should().BeAssignableTo<IIdFactory>();
        }
    }
}