using Chat.Application.Models;
using Chat.Application.UnitTests.TestSupport;
using FluentAssertions;

namespace Chat.Application.UnitTests.Models.MessageDtoTests;

public static class CtorTests
{
    public class Should_Instantiate_MessageDto
        : Given_When_Then
    {
        private string _formattedText = null!;
        private MessageDto _sut = null!;
        private DateTime _createdOn;
        private string _createdBy = null!;
        private string _text = null!;

        protected override void Given()
        {
            _createdOn = new DateTime(2022, 1, 1);
            _createdBy = "bar";
            _text = "biz";
            _formattedText = "bar biz";
        }

        protected override void When()
        {
            _sut = new MessageDto(_createdOn, _createdBy, _text, _formattedText);
        }

        [Fact]
        public void Then_It_Should_Have_A_Valid_Instance()
        {
            _sut.Should().NotBeNull();
        }

        [Fact]
        public void Then_It_Should_Have_The_CreatedOn()
        {
            _sut.CreatedOn.Should().Be(_createdOn);
        }
        
        [Fact]
        public void Then_It_Should_Have_The_CreatedBy()
        {
            _sut.CreatedBy.Should().Be(_createdBy);
        }
        
        [Fact]
        public void Then_It_Should_Have_The_Text()
        {
            _sut.Text.Should().Be(_text);
        }
        
        [Fact]
        public void Then_It_Should_Have_The_FormattedText()
        {
            _sut.FormattedText.Should().Be(_formattedText);
        }
    }
}