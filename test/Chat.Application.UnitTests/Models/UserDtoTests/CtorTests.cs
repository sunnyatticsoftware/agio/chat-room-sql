using Chat.Application.Models;
using Chat.Application.UnitTests.TestSupport;
using FluentAssertions;

namespace Chat.Application.UnitTests.Models.UserDtoTests;

public static class CtorTests
{
    public class Should_Instantiate_UserDto
        : Given_When_Then
    {
        private string _username = null!;
        private string _connectionId = null!;
        private UserDto _sut = null!;

        protected override void Given()
        {
            _username = "foo";
            _connectionId = "bar";
        }

        protected override void When()
        {
            _sut = new UserDto(_username, _connectionId);
        }

        [Fact]
        public void Then_It_Should_Have_A_Valid_Instance()
        {
            _sut.Should().NotBeNull();
        }

        [Fact]
        public void Then_It_Should_Have_The_Expected_Username()
        {
            _sut.Username.Should().Be(_username);
        }
        
        [Fact]
        public void Then_It_Should_Have_The_Expected_ConnectionId()
        {
            _sut.ConnectionId.Should().Be(_connectionId);
        }
    }
}