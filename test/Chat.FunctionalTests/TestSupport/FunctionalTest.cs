using Chat.Infra.Repository.SqlServer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;

namespace Chat.FunctionalTests.TestSupport;

public abstract class FunctionalTest
    : IDisposable
{
    protected HttpClient HttpClient { get; }
    
    private readonly string _uniqueDatabaseName;
    private readonly IServiceProvider _serviceProvider;

    public FunctionalTest()
    {
        var server =
            new TestServer(
                new WebHostBuilder()
                    .UseStartup<Startup>()
                    .UseEnvironment("Test")
                    .UseCommonConfiguration()
                    .ConfigureTestServices(ConfigureTestServices));

        HttpClient = server.CreateClient();

        _serviceProvider = server.Services;
        
        // Apply Migrations
        _uniqueDatabaseName = $"Test-{Guid.NewGuid()}";
        using var dbContext = _serviceProvider.CreateScope().ServiceProvider.GetRequiredService<ChatRoomDbContext>();
        dbContext.Database.Migrate();
    }

    protected virtual void ConfigureTestServices(IServiceCollection services)
    {
        // remove EF Core registration
        var dbContextDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<ChatRoomDbContext>));
        if (dbContextDescriptor != null)
        {
            services.Remove(dbContextDescriptor);
        }

        services.AddDbContext<ChatRoomDbContext>(
            (sp, options) =>
            {
                var configuration = sp.GetRequiredService<IConfiguration>();
                var testConnectionString = configuration.GetValue<string>("ConnectionStrings:ChatsDatabase");
                var parts = testConnectionString.Split(";");
                var uniqueDbTestConnectionStringBuilder = new StringBuilder();
                foreach (var part in parts)
                {
                    var isDatabasePart = part.StartsWith("Database=");
                    uniqueDbTestConnectionStringBuilder.Append(isDatabasePart
                        ? $"Database={_uniqueDatabaseName};"
                        : $"{part};");
                }

                var uniqueDbTestConnectionString = uniqueDbTestConnectionStringBuilder.ToString().TrimEnd(';');
                options.UseSqlServer(uniqueDbTestConnectionString);
            });
        
        // remove Dapper SqlConnection's registration
        var sqlConnectionDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(SqlConnection));
        if (sqlConnectionDescriptor != null)
        {
            services.Remove(sqlConnectionDescriptor);
        }
        
        services.AddTransient(
            sp =>
            {
                var configuration = sp.GetRequiredService<IConfiguration>();
                var testConnectionString = configuration.GetValue<string>("ConnectionStrings:ChatsDatabase");
                var parts = testConnectionString.Split(";");
                var uniqueDbTestConnectionStringBuilder = new StringBuilder();
                foreach (var part in parts)
                {
                    var isDatabasePart = part.StartsWith("Database=");
                    uniqueDbTestConnectionStringBuilder.Append(isDatabasePart
                        ? $"Database={_uniqueDatabaseName};"
                        : $"{part};");
                }

                var uniqueDbTestConnectionString = uniqueDbTestConnectionStringBuilder.ToString().TrimEnd(';');
                
                var sqlConnection = new SqlConnection(uniqueDbTestConnectionString);
                return sqlConnection;
            });
    }

    public void Dispose()
    {
        using var dbContext = _serviceProvider.GetService<ChatRoomDbContext>();
        dbContext?.Database.EnsureDeleted();
    }
}