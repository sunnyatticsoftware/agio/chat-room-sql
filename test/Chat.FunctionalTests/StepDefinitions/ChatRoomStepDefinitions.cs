using Chat.FunctionalTests.TestSupport;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace Chat.FunctionalTests.StepDefinitions;

[Binding]
public class ChatRoomStepDefinitions
    : FunctionalTest
{
    private HttpResponseMessage _result = null!;

    [When(@"creating a chat room")]
    public async Task WhenCreatingAChatRoom()
    {
        _result = await HttpClient.PostAsync("api/chatrooms", null);
    }

    [Then(@"the chat room should have valid id")]
    public async Task ThenTheChatRoomShouldHaveValidId()
    {
        var responseText = await _result.Content.ReadAsStringAsync(); // e.g: "0e45de22-04f3-40e5-b6c9-93c277494a91"
        responseText = responseText.Replace("\"", string.Empty);
        var isValidGuid = Guid.TryParse(responseText, out var id);
        isValidGuid.Should().BeTrue();
    }
}